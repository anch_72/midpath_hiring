from api.db import exec_to_json
from api.mixins import register, BaseMixin

@register
class Sales(BaseMixin):
    # Base
    slug = "sales"

    @classmethod
    def handle_get_list(cls, *, kwargs):
        return exec_to_json("select 1 as c1, 2 as c2")

@register
class Inventory(BaseMixin):
    slug = "inv"

    @classmethod
    def handle_post_list(cls, *, kwargs):
        n=kwargs['item_name']
        q=kwargs['quantity']
        text = "Insert into Inventory(item_name,quantity) values(:item_name, :quantity)"
        return exec_to_json(text,item_name=n, quantity=q, returns_rows=False)

    @classmethod
    def handle_get_list(cls, *, kwargs):
        return exec_to_json("select * from Inventory")

    @classmethod
    def handle_post_delete(cls, *, kwargs):
        n=kwargs['item_name']
        text = "Delete from Inventory where item_name=:item_name"
        return exec_to_json(text,item_name=n, returns_rows=False)
    
    @classmethod
    def handle_post_update(cls, *, kwargs):
        n=kwargs['item_name']
        q=kwargs['quantity']
        text = "Update Inventory set quantity=:quantity where item_name=:item_name"
        return exec_to_json(text,item_name=n,quantity=q, returns_rows=False)

